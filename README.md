# Pulsechain Contract Verification Framework

## How to use
1. Clone this repository
2. Install `yarn` (Google it if you have no clue what this is)
3. Run `yarn install` to install dependencies
4. Add your contracts inside the `contracts` directory.
5. Modify the `hardhat.config.js` file as needed.
6. Modify the `scripts/verify.js` script for your contract.
7. Run `yarn verify`